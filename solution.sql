-- ACTIVITY FOR SESSION 5 -

-- 1. Return the customerName of the customers who are from the Philippines

SELECT customerName FROM customers WHERE country='Philippines';

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"

SELECT contactLastName, contactFirstName FROM customers WHERE customerName='La Rochelle Gifts';

-- 3. Return the product name and MSRP of the product named "The Titanic"

SELECT productName, MSRP FROM products WHERE productName='The Titanic';

-- 4. Return the first and last name of the employee whose email is "jfirrell@classicmodelcars.com"

SELECT firstName, lastName FROM employees WHERE email='jfirrelli@classicmodelcars.com';

-- 5. Return the names of customers who have no registered state

SELECT customerName FROM customers WHERE state IS NULL;

-- 6. Return the first name, last name, email of the employee who last name is Patterson and first name is Steve

SELECT firstName, lastName, email FROM employee WHERE lastName="Patterson" AND firstName="Steve";

-- 7. Return customer name, country, and credit limit of customers whose countires are NOT USA and whose credit limits are greater than 3000

SELECT customerName, country, creditLimit FROM customers WHERE NOT country = "USA" AND creditLimit > 3000;

-- 8. Return the customer numbers of orders whose comments contain the string DHL

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 9. Return the product lines whose text description mentions the phrase 'state of the art'

SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 10. Return the countries of customers with duplication

SELECT DISTINCT country FROM customers;

-- 11. Return the statuses of orders without duplication

SELECT DISTINCT status FROM orders;

-- 12. Return the customer names and countries of customers whose country is USA, France, or Canda

SELECT customerName, country FROM customers
WHERE country="USA" OR country="France" OR country="Canada";

-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo

SELECT firstName,lastName,city FROM employees JOIN offices ON employees.officeCode = offices.officeCode WHERE city="Tokyo";

-- 14. Return the customer names of customers who were served  by the employee named "Leslie Thompson"

SELECT customerName FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE firstName="Leslie" AND lastName="Thompson";

-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"

SELECT productName, customerName FROM products JOIN orderdetails ON products.productCode = orderdetails.productCode JOIN orders ON orderdetails.orderNumber = orders.orderNumber JOIN customers ON orders.customerNumber = customers.customerNumber WHERE customerName = "Baane Mini Imports"

-- 16. Return the employees' first names, employees' last names, customers' names, and offices'countries of transactions whose customers and offices are in the same country

SELECT firstName, lastName, customerName, offices.country FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber JOIN offices ON employees.officeCode = offices.officeCode WHERE offices.country = customers.country;

-- 17. Return the product name and quantity stock of products that belong to the product line "planes" with stock quantities less than 1000.

SELECT productName, quantityInStock FROM products WHERE productLine="Planes" AND quantityInStock < 1000;

-- 18. Return the customer's name with a phone number containing "+81"

SELECT customerName FROM customers WHERE phone LIKE "%+81%"

-- Stretch Goals
-- 1. Return the product name of the orders where customer name is Baane Mini Imports

SELECT productName FROM products JOIN orderdetails ON products.productCode = orderdetails.productCode JOIN orders ON orderdetails.orderNumber = orders.orderNumber JOIN customers ON orders.customerNumber = customers.customerNumber WHERE customerName = "Baane Mini Imports";

-- 2. Return the last name and first name of employees that reports to Anthony Bow

SELECT lastName, firstName FROM employees WHERE reportsTo IN (
    SELECT employeeNumber FROM employees WHERE lastName = "Bow" AND firstName = "Anthony"
);

-- 3. Return the product name of the product with the maximum MSRP

SELECT productName FROM products WHERE MSRP IN (
    SELECT MAX(MSRP) FROM products
);

-- 4. Return the number of products group by productline

SELECT COUNT(*), productLine
FROM products
GROUP BY productLine

-- 5. Return the number of products where the status is cancelled

SELECT COUNT(*) FROM products WHERE productCode IN
    ( SELECT DISTINCT products.productCode FROM products JOIN orderdetails ON products.productCode = orderdetails.productCode JOIN orders ON orderdetails.orderNumber = orders.orderNumber 
    WHERE status = "Cancelled" )
